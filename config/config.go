package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

//Config is config
type LocalConfig struct {
	JobCount   int      `json:"job_count"`
	BuildCount int      `json:"build_count"`
	Tags       []string `json:"tags"`
	Hostname   string
	ClusterID  string   `json:"cluster_id"`
	MQAddress  string   `json:"mq_addr"`
}

func GetDefaultLocalConfig() (config LocalConfig) {

	ConfigJSON, rErr := ioutil.ReadFile("/etc/judge/config.json")

	if rErr != nil {
		log.Panicln(rErr)
	}

	jErr := json.Unmarshal(ConfigJSON, &config)

	if jErr != nil {
		log.Panicln(jErr)
	}

	config.Hostname, _ = os.Hostname()

	return config
}
