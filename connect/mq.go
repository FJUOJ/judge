package connect

import (
	"github.com/nats-io/stan.go"
	"log"
)

var sc stan.Conn

func NewMQ(clusterID, clientID, uri string) stan.Conn {

	var err error

	sc, err = stan.Connect(clusterID, clientID, stan.NatsURL(uri))

	if err != nil {
		log.Fatal(err)
	}

	return sc

}

func CloseMQ() {
	sc.Close()
}

func GetMQConn() stan.Conn {
	return sc
}
