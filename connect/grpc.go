package connect

import (
	"gitlab.com/fjuoj/protobuf/chief"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"

	"gitlab.com/fjuoj/protobuf/judge"
)

var server *grpc.Server
var listen net.Listener

func init() {

	initServer()

}

func initServer() {

	var err error

	listen, err = net.Listen("tcp", ":9487")
	if err != nil {
		log.Fatalf("建立監聽端口失敗：%v", err)
	}

	server = grpc.NewServer()
}

func InitJudgePoolServer(srv judge.JudgePoolServer) {
	judge.RegisterJudgePoolServer(server, srv)
}

func NewChiefBuildClient(chiefConn *grpc.ClientConn) chief.BuildClient {
	return chief.NewBuildClient(chiefConn)
}

func NewChiefJobClient(chiefConn *grpc.ClientConn) chief.JobClient {
	return chief.NewJobClient(chiefConn)
}

func GRPCServe() error {

	reflection.Register(server)
	return server.Serve(listen)
}

func CloseGRPCServer() {
	server.GracefulStop()
}
