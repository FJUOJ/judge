package connect

import (
	"github.com/docker/docker/client"
)

var cli *client.Client

func NewDocker() {

	var err error

	cli, err = client.NewClientWithOpts(client.WithAPIVersionNegotiation())
	if err != nil {
		panic(err)
	}

}

func CloseDocker() error {
	return cli.Close()
}

func GetDockerCli() *client.Client {
	return cli
}
