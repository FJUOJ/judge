package connect

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"

	"github.com/minio/minio-go/v6"
)

var minioClient *minio.Client

func NewS3(endpoint, accessKeyID, secretAccessKey string, useSSL bool) {

	var err error

	minioClient, err = minio.New(endpoint, accessKeyID, secretAccessKey, useSSL)
	if err != nil {
		log.Fatalln(err)
	}
}

func GetS3Conn() *minio.Client {
	return minioClient
}

func S3UploadToBucket(bucket, object string, file *os.File) error {

	fileInfo, err := file.Stat()
	if err != nil {
		return err
	}

	_, err = minioClient.PutObject(bucket, object, file, fileInfo.Size(), minio.PutObjectOptions{
		ContentType:  "text/plain",
		CacheControl: "no-cache",
		NumThreads:   uint(runtime.NumCPU() / 2),
	})

	return err
}

func S3GetFileFromBucketAndWriteToFile(bucket, object string) error {

	// download the file
	tmpReader, err := minioClient.GetObject(bucket, object, minio.GetObjectOptions{})
	if err != nil {
		return err
	}

	defer func() {
		if err := tmpReader.Close(); err != nil {
			panic(err)
		}
	}()

	// open output file
	tmpFile, err := os.Create(fmt.Sprintf("/tmp/judge/%s/%s", bucket, object))

	if err != nil {
		return err
	}

	// close fo on exit and check for its returned error
	defer func() {
		if err := tmpFile.Close(); err != nil {
			panic(err)
		}
	}()

	w := bufio.NewWriter(tmpFile)

	// make a buffer to keep chunks that are read
	buf := make([]byte, 1024)
	for {
		// read a chunk
		n, err := tmpReader.Read(buf)
		if err != nil && err != io.EOF {
			return err
		}
		if n == 0 {
			break
		}

		// write a chunk
		if _, err := w.Write(buf[:n]); err != nil {
			return err
		}
	}

	if err = w.Flush(); err != nil {
		return err
	}

	return nil
}
