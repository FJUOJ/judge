package main

import (
	"gitlab.com/fjuoj/judge/config"
	"gitlab.com/fjuoj/judge/connect"
	"gitlab.com/fjuoj/judge/pool"
)

var localConfig config.LocalConfig
var p pool.Pool

func main() {

	localConfig = config.GetDefaultLocalConfig()
	p.SetCount(localConfig.JobCount, localConfig.BuildCount)

	connect.NewDocker()
	defer connect.CloseDocker()

	connect.NewMQ(localConfig.ClusterID, localConfig.Hostname, localConfig.MQAddress)
	defer connect.CloseMQ()

	srv := HandleServiceEcho()
	defer srv.Unsubscribe()

	remoteSecret := GetServiceSecret()
	p.SetSecret(remoteSecret)

	connect.NewS3(remoteSecret.GetS3Endpoint(), remoteSecret.GetS3KeyId(), remoteSecret.GetS3SecretAccessKey(), remoteSecret.GetS3UseSsl())
	connect.InitJudgePoolServer(&p)

	jobSubmitSrv := p.HandleJobSubmitViaMQ()
	defer jobSubmitSrv.Unsubscribe()
	buildSubmitSrv := p.HandleBuildSubmitViaMQ()
	defer buildSubmitSrv.Unsubscribe()
	jobStatusSrv := p.HandleJobFollowViaMQ()
	defer jobStatusSrv.Unsubscribe()
	buildStatusSrv := p.HandleBuildFollowViaMQ()
	defer buildStatusSrv.Unsubscribe()

	connect.GRPCServe()
	defer connect.CloseGRPCServer()

}
