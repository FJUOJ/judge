package main

import (
	"github.com/gogo/protobuf/proto"
	"github.com/nats-io/stan.go"
	"github.com/thanhpk/randstr"
	"gitlab.com/fjuoj/judge/connect"
	"gitlab.com/fjuoj/protobuf/judge"
	"gitlab.com/fjuoj/protobuf/service"
	"log"
)

func HandleServiceEcho() stan.Subscription {

	srv, err := connect.GetMQConn().Subscribe("online-judge", func(m *stan.Msg) {

		req := &service.JudgeServiceRequest{}
		if err := proto.Unmarshal(m.Data, req); err != nil {
			return
		}

		//check tags
		if subslice(localConfig.Tags, req.GetTags()) == false {
			return
		}

		//check usage
		if req.GetUsage() == service.JudgeServiceRequest_JOB_REQ {
			if p.GetFreeJob() <= 0 {
				return
			}
		}
		if req.GetUsage() == service.JudgeServiceRequest_BUILD_REQ {
			if p.GetFreeBuilder() <= 0 {
				return
			}
		}

		srvInfo := service.JudgeServiceInfo{
			Hostname:  localConfig.Hostname,
			FreeJob:   int64(p.GetFreeJob()),
			FreeBuild: int64(p.GetFreeBuilder()),
			JobCap:    int64(localConfig.JobCount),
			BuildCap:  int64(localConfig.BuildCount),
			Tags:      localConfig.Tags,
		}

		if data, err := proto.Marshal(&srvInfo); err != nil {
			return
		} else {
			connect.GetMQConn().Publish(m.Reply, data)
		}

	})

	if err != nil {
		log.Fatal(err)
	}

	return srv

}

func GetServiceSecret() judge.JudgeSecret {

	ch := make(chan judge.JudgeSecret)
	defer close(ch)

	replySub := randstr.Hex(32)
	sub, err := connect.GetMQConn().Subscribe(replySub, func(m *stan.Msg) {

		resp := judge.JudgeSecret{}
		if err := proto.Unmarshal(m.Data, &resp); err != nil {
			return
		}

		ch <- resp
	})

	if err != nil {
		log.Fatalln(err)
	}

	defer sub.Unsubscribe()

	info := service.JudgeServiceInfo{
		Hostname: localConfig.Hostname,
		JobCap:   int64(localConfig.JobCount),
		BuildCap: int64(localConfig.BuildCount),
		Tags:     localConfig.Tags,
	}

	reqByte, err := proto.Marshal(&info)

	if err != nil {
		log.Fatalln(err)
	}

	if err := connect.GetMQConn().Publish("judge-secret-req", reqByte); err != nil {
		log.Fatalln(err)
	}

	return <-ch

}