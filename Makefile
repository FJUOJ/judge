ver=release

ROOTDIR=$(shell pwd)

OUTDIR=.
OUTFILE=judge

ALL:
	CGO_ENABLED=0 go build --ldflags "-extldflags -static" -v -p 4 -o $(OUTDIR)/$(OUTFILE) ./*.go
install: ALL
	mv $(OUTDIR)/$(OUTFILE) /usr/bin
	cp config/config.example.json /etc/judge/config.json
clean:
	rm -f $(OUTDIR)/$(OUTFILE)