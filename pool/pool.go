package pool

import (
	"context"
	"errors"
	"github.com/gogo/protobuf/proto"
	"github.com/nats-io/stan.go"
	"gitlab.com/fjuoj/judge/connect"
	"gitlab.com/fjuoj/protobuf/judge"
	"gitlab.com/fjuoj/protobuf/oj"
	"log"
	"sync"
)

var secret judge.JudgeSecret

type Pool struct {
	Builder []Build
	Job     []Job
}

func (p *Pool) JobSubmit(ctx context.Context, req *oj.JobRequest) (*oj.SuccessReply, error) {

	reply := oj.SuccessReply{OK: false}

	for i := range p.Job {
		if p.Job[i].RunAsync(req) == nil {
			reply.OK = true
			return &reply, nil
		}
	}

	return &reply, errors.New("full job")
}

func (p *Pool) JobStatusFollow(ctx context.Context, req *judge.RequestID) (*oj.JobStatus, error) {

	reply := oj.JobStatus{
		JobId:  req.GetId(),
		Status: "",
		Done:   false,
	}

	for i := range p.Job {
		if p.Job[i].GetID() == req.GetId() {

			if p.Job[i].GetAvailable() == true {
				reply.Done = true
			}

			reply.Status = p.Job[i].GetStatus()
			return &reply, nil
		}
	}

	return nil, errors.New("can't find on this host")
}

func (p *Pool) BuildSubmit(ctx context.Context, req *oj.BuildRequest) (*oj.SuccessReply, error) {

	reply := oj.SuccessReply{OK: false}

	for i := range p.Builder {
		if p.Builder[i].RunAsync(req) == nil {
			reply.OK = true
			return &reply, nil
		}
	}

	return &reply, errors.New("full build")

}

func (p *Pool) BuildStatusFollow(ctx context.Context, req *judge.RequestID) (*oj.BuildStatus, error) {

	reply := oj.BuildStatus{
		BuildId: req.GetId(),
		Done:    false,
		Status:  "",
	}

	for i := range p.Builder {
		if p.Builder[i].GetID() == req.GetId() {

			if p.Builder[i].GetAvailable() == true {
				reply.Done = true
			}

			reply.Status = p.Builder[i].GetStatus()
			return &reply, nil
		}
	}

	return nil, errors.New("can't find on this host")

}

func (p *Pool) GetFreeJob() (n int) {

	for i := range p.Job {
		if p.Job[i].GetAvailable() == true {
			n++
		}
	}

	return n
}

func (p *Pool) GetFreeBuilder() (n int) {

	for i := range p.Builder {
		if p.Builder[i].GetAvailable() == true {
			n++
		}
	}

	return n
}

func (p *Pool) SetCount(jobCount int, builderCount int) {

	p.Builder = make([]Build, builderCount)

	for i := range p.Builder {
		p.Builder[i] = Build{
			available:     true,
			availableLock: sync.Mutex{},
			info:          nil,
			status:        "init",
		}
	}

	p.Job = make([]Job, jobCount)

	for i := range p.Job {
		p.Job[i] = Job{
			available:     true,
			availableLock: sync.Mutex{},
			info:          nil,
			status:        "init",
		}
	}
}

func (p Pool) SetSecret(sec judge.JudgeSecret) {
	secret = sec
}

func (p *Pool)HandleJobSubmitViaMQ() stan.Subscription {

	srv, err := connect.GetMQConn().QueueSubscribe("job-new","queue", func(m *stan.Msg) {//TODO: handle queue-name

		req := &oj.JobRequest{}
		if err := proto.Unmarshal(m.Data, req); err != nil {
			return
		}

		if _, err := p.JobSubmit(context.TODO(), req); err != nil {
			return
		} else {
			m.Ack()
		}

	}, stan.SetManualAckMode())

	if err != nil {
		log.Fatal(err)
	}

	return srv

}

func (p *Pool)HandleBuildSubmitViaMQ() stan.Subscription {

	srv, err := connect.GetMQConn().QueueSubscribe("build-new","queue", func(m *stan.Msg) {//TODO: handle queue-name

		req := &oj.BuildRequest{}
		if err := proto.Unmarshal(m.Data, req); err != nil {
			return
		}

		if _, err := p.BuildSubmit(context.TODO(), req); err != nil {
			return
		} else {
			m.Ack()
		}

	}, stan.SetManualAckMode())

	if err != nil {
		log.Fatal(err)
	}

	return srv

}

func (p *Pool)HandleJobFollowViaMQ() stan.Subscription {

	srv, err := connect.GetMQConn().Subscribe("job-follow", func(m *stan.Msg) {

		req := &judge.RequestID{}
		if err := proto.Unmarshal(m.Data, req); err != nil {
			return
		}

		if stat, err := p.JobStatusFollow(context.TODO(), req); err != nil {
			return
		} else {

			statByte, err := proto.Marshal(stat)
			if err != nil {
				return
			}
			connect.GetMQConn().Publish(m.Reply, statByte)
		}
	})

	if err != nil {
		log.Fatal(err)
	}

	return srv
}

func (p *Pool)HandleBuildFollowViaMQ() stan.Subscription {

	srv, err := connect.GetMQConn().Subscribe("build-follow", func(m *stan.Msg) {

		req := &judge.RequestID{}
		if err := proto.Unmarshal(m.Data, req); err != nil {
			return
		}

		if stat, err := p.BuildStatusFollow(context.TODO(), req); err != nil {
			return
		} else {

			statByte, err := proto.Marshal(stat)
			if err != nil {
				return
			}
			connect.GetMQConn().Publish(m.Reply, statByte)
		}
	})

	if err != nil {
		log.Fatal(err)
	}

	return srv
}
