package pool

import (
	"context"
	"errors"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/gogo/protobuf/proto"
	"gitlab.com/fjuoj/judge/connect"
	"gitlab.com/fjuoj/protobuf/oj"
	"log"
	"os"
	"sync"
	"time"
)

type Build struct {
	available     bool
	availableLock sync.Mutex
	info          *oj.BuildRequest
	status        string
}

func (b *Build) updateStatus(status string) {

	b.availableLock.Lock()
	defer b.availableLock.Unlock()

	b.status = status
	log.Printf("%s: %s", b.GetID(), status)

	s := &oj.BuildStatus{
		BuildId: b.info.GetBuildId(),
		Status:  b.status,
	}

	data, _ := proto.Marshal(s)
	connect.GetMQConn().Publish("build-status", data)
}

func (b *Build) GetStatus() string {

	return b.status
}

func (b *Build) GetID() string {

	return b.info.GetBuildId()
}

func (b *Build) GetAvailable() bool {

	if b.available == true {
		return true
	}

	return false
}

func (b *Build) setAvailable(v bool) {

	b.availableLock.Lock()
	defer b.availableLock.Unlock()

	b.available = v

}

func (b *Build) run() (err error) {

	var tarFile *os.File
	timeTotalStart := time.Now()

	defer tarFile.Close()
	defer b.setAvailable(true)
	defer func(err error) {
		if err != nil {
			b.updateStatus("error while compiling code" + err.Error())
			if resultByte, err := proto.Marshal(&oj.BuildResult{
				BuildId:             b.GetID(),
				Success:             false,
				TarSize:             -1,
				TotalTimeNanosecond: time.Since(timeTotalStart).Nanoseconds(),
			}); err != nil {
				b.updateStatus("something wrong at protobuf marshal, " + err.Error())
			} else {
				if err = connect.GetMQConn().Publish("build-result", resultByte); err != nil {
					b.updateStatus("something wrong at MQ server, " + err.Error())
				}
			}
		}

	}(err)

	b.updateStatus("getting template from s3")
	if err = connect.S3GetFileFromBucketAndWriteToFile("template", b.info.GetTemplateId()); err != nil {
		return err
	}

	b.updateStatus("getting code from s3")
	if err = connect.S3GetFileFromBucketAndWriteToFile("code", b.info.GetCodeId()); err != nil {
		return err
	}

	b.updateStatus("assembling code and template")
	//code
	codeSrc := fmt.Sprintf("/tmp/judge/code/%s", b.info.GetCodeId())
	codeDst := fmt.Sprintf("/tmp/judge/build/%s/", b.info.GetBuildId())
	if err = unZip(codeSrc, codeDst); err != nil {
		return err
	}

	//template
	tmpSrc := fmt.Sprintf("/tmp/judge/template/%s", b.info.GetTemplateId())
	tmpDst := fmt.Sprintf("/tmp/judge/build/%s/Dockerfile", b.info.GetBuildId())
	if err = copyFileTo(tmpSrc, tmpDst); err != nil {
		return err
	}

	//tar folder
	var tarSize int64
	tarFile, err = os.Create(fmt.Sprintf("/tmp/judge/build/%s.tar", b.info.GetBuildId()))
	if err != nil {
		return err
	}

	if err = tarFolder("/tmp/judge/build/", tarFile); err != nil {
		return err
	}

	if tarInfo, err := tarFile.Stat(); err != nil {
		return err
	} else {
		tarSize = tarInfo.Size()
	}

	b.updateStatus("reading code from s3")

	codeStat, err := os.Stat(codeSrc)
	if err != nil {
		return err
	}

	b.updateStatus("start building image in docker")
	ctx, _ := context.WithTimeout(context.TODO(), time.Nanosecond*time.Duration(b.info.Problem.GetBuildTimeLimitNs()))
	bts := time.Now()
	if _, err = connect.GetDockerCli().ImageBuild(ctx, tarFile, types.ImageBuildOptions{
		Tags:        []string{b.GetID()},
		NetworkMode: "none",
	}); err != nil {
		return err
	}

	<-ctx.Done()
	buildTime := time.Since(bts)
	if err = ctx.Err(); err != nil {
		return err
	}

	b.updateStatus("tag an image")
	tag := fmt.Sprintf("%s/%s", secret.RegistryUri, b.GetID())
	if err = connect.GetDockerCli().ImageTag(context.TODO(), b.GetID(), tag); err != nil {
		return err
	}

	b.updateStatus("uploading image to registry")
	if _, err = connect.GetDockerCli().ImagePush(context.TODO(), tag,
		types.ImagePushOptions(types.ImagePullOptions{
			All:          true,
			RegistryAuth: secret.RegistryToken,
		},
		)); err != nil {
		return err
	}

	b.updateStatus("reporting result to chief")
	if resultByte, err := proto.Marshal(&oj.BuildResult{
		BuildId:          b.GetID(),
		Success:          true,
		TarSize:          tarSize,
		BuildTimeUsageNs: buildTime.Nanoseconds(),
		CodeSizeBytes:    codeStat.Size(),

		TotalTimeNanosecond: time.Since(timeTotalStart).Nanoseconds(),
	}); err != nil {
		b.updateStatus("something wrong at protobuf marshal, " + err.Error())
	} else {
		if err = connect.GetMQConn().Publish("build-result", resultByte); err != nil {
			b.updateStatus("something wrong at MQ server, " + err.Error())
		}
	}

	b.updateStatus("done")
	return nil
}

func (b *Build) RunSync(info *oj.BuildRequest) (err error) {

	if b.GetAvailable() != true {
		return errors.New("already running Job")
	}

	b.setAvailable(false)
	b.updateStatus("init")
	b.info = info

	return b.run()
}

func (b *Build) RunAsync(info *oj.BuildRequest) (err error) {

	if b.GetAvailable() != true {
		return errors.New("already running Job")
	}

	b.setAvailable(false)
	b.updateStatus("init")
	b.info = info

	go b.run()
	return nil
}
