package pool

import (
	"context"
	"crypto/sha256"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/api/types/volume"
	"github.com/gogo/protobuf/proto"
	"gitlab.com/fjuoj/judge/connect"
	"gitlab.com/fjuoj/protobuf/oj"
	"io/ioutil"
	"os"
	"sync"
	"time"
)

type Job struct {
	available     bool
	availableLock sync.Mutex
	info          *oj.JobRequest
	status        string
}

type InfoJSON struct {
	RealTime   time.Duration `json:"real_time"`
	UserTime   time.Duration `json:"user_time"`
	SystemTime time.Duration `json:"sys_time"`
	MaxRSS     int64         `json:"max_rss"`
}

func (j *Job) updateStatus(status string) {

	j.availableLock.Lock()
	defer j.availableLock.Unlock()

	j.status = status

	s := &oj.JobStatus{
		JobId:  j.info.GetJobId(),
		Status: status,
	}

	data, _ := proto.Marshal(s)
	connect.GetMQConn().Publish("job-status", data)
}

func (j *Job) GetStatus() string {

	return j.status
}

func (j *Job) GetID() string {

	return j.info.GetJobId()
}

func (j *Job) GetAvailable() bool {

	if j.available == true {
		return true
	}

	return false
}

func (j *Job) setAvailable(v bool) {

	j.availableLock.Lock()
	defer j.availableLock.Unlock()

	j.available = v

}

func (j *Job) run() (err error) {

	timeTotalStart := time.Now()

	defer j.setAvailable(true)
	defer func() {
		if err != nil {
			j.updateStatus("error while running code" + err.Error())
			if resultByte, err := proto.Marshal(&oj.JobResult{
				JobId:               j.GetID(),
				Success:             false,
				TotalTimeNanosecond: time.Since(timeTotalStart).Nanoseconds(),
			}); err != nil {
				j.updateStatus("something wrong at protobuf marshal, " + err.Error())
			} else {
				if err = connect.GetMQConn().Publish("job-result", resultByte); err != nil {
					j.updateStatus("something wrong at MQ server, " + err.Error())
				}
			}
		}
	}()

	j.updateStatus("getting image from registry")
	tag := fmt.Sprintf("%s/%s", secret.GetRegistryUri(), j.info.GetImage())
	if _, err := connect.GetDockerCli().ImagePull(context.TODO(), tag, types.ImagePullOptions{
		All:          true,
		RegistryAuth: secret.GetRegistryToken(),
	}); err != nil {
		return err
	}

	j.updateStatus("getting input from s3")
	if err = connect.S3GetFileFromBucketAndWriteToFile("input", j.info.GetTask().GetInputId()); err != nil {
		return err
	}

	j.updateStatus("creating volume, mapping input and output and time")
	var vol types.Volume
	vol, err = connect.GetDockerCli().VolumeCreate(context.TODO(), volume.VolumeCreateBody{})
	defer connect.GetDockerCli().VolumeRemove(context.Background(), vol.Name, true)

	inSrc := fmt.Sprintf("/tmp/judge/input/%s", j.info.GetTask().GetInputId())
	inDst := fmt.Sprintf("%s/in", vol.Mountpoint)
	if err = copyFileTo(inSrc, inDst); err != nil {
		return err
	}

	j.updateStatus("creating container")
	var contCreateBody container.ContainerCreateCreatedBody
	createCtx, _ := context.WithTimeout(context.TODO(), time.Second*10)
	cts := time.Now()
	if contCreateBody, err = connect.GetDockerCli().ContainerCreate(createCtx, &container.Config{
		Hostname:        j.GetID(),
		Cmd:             j.info.GetCmd(),
		Image:           tag,
		NetworkDisabled: true,
	}, &container.HostConfig{
		Binds: []string{fmt.Sprintf("%s:%s", vol.Name, "/opt/")},
		Resources: container.Resources{
			Memory:             j.info.GetTask().GetRunMemoryLimitByte(),
			CPURealtimeRuntime: j.info.GetTask().GetRunTimeLimitNs() * 2 * 1000 * 1000,
		},
	}, &network.NetworkingConfig{}, j.GetID()); err != nil {
		return err
	}

	<-createCtx.Done()
	createTime := time.Since(cts)
	if err = createCtx.Err(); err != nil {
		return err
	}

	defer connect.GetDockerCli().ContainerRemove(context.Background(), contCreateBody.ID, types.ContainerRemoveOptions{
		RemoveVolumes: true,
		RemoveLinks:   true,
		Force:         true,
	})

	j.updateStatus("running container")
	runCtx, _ := context.WithTimeout(context.TODO(), time.Nanosecond*time.Duration(j.info.GetTask().GetRunTimeLimitNs())*3)
	rts := time.Now()
	if err = connect.GetDockerCli().ContainerStart(runCtx, contCreateBody.ID, types.ContainerStartOptions{}); err != nil {
		return err
	}

	<-runCtx.Done()
	runOutTime := time.Since(rts)
	if err = runCtx.Err(); err != nil {
		return err
	}

	j.updateStatus("uploading output to s3")

	var infoFile, outFile *os.File
	var infoByte, outByte []byte
	defer infoFile.Close()
	defer outFile.Close()

	if infoFile, err = os.Open(fmt.Sprintf("%s/info", vol.Mountpoint)); err != nil {
		return err
	}
	if err = connect.S3UploadToBucket("time", j.GetID(), infoFile); err != nil {
		return err
	}

	if outFile, err = os.Open(fmt.Sprintf("%s/out", vol.Mountpoint)); err != nil {
		return err
	}
	if err = connect.S3UploadToBucket("out", j.GetID(), outFile); err != nil {
		return err
	}

	j.updateStatus("calculating sha256")
	outInfo, _ := outFile.Stat()
	if outByte, err = ioutil.ReadAll(outFile); err != nil {
		return err
	}

	hashByte := sha256.Sum256(outByte)
	hash := fmt.Sprintf("%x", hashByte[:])

	j.updateStatus("understanding the output")
	var infoJSON InfoJSON
	if infoByte, err = ioutil.ReadAll(infoFile); err != nil {
		return err
	}

	if err = json.Unmarshal(infoByte, &infoJSON); err != nil {
		return err
	}

	j.updateStatus("reporting result to chief")

	if resultByte, err := proto.Marshal(&oj.JobResult{
		JobId:                            j.GetID(),
		Success:                          true,
		OutSize:                          outInfo.Size(),
		CreateTimeNanosecond:             createTime.Nanoseconds(),
		RunTimeInsideContainerNanosecond: infoJSON.RealTime.Nanoseconds(),
		RunTimeOutContainerNanosecond:    runOutTime.Nanoseconds(),
		SysTimeNanosecond:                infoJSON.SystemTime.Nanoseconds(),
		UserTimeNanosecond:               infoJSON.UserTime.Nanoseconds(),
		TotalTimeNanosecond:              time.Since(timeTotalStart).Nanoseconds(),
		RunMemoryUsageByte:               infoJSON.MaxRSS,
		OutHashSha256:                    hash,
	}); err != nil {
		j.updateStatus("something wrong at protobuf marshal, " + err.Error())
	} else {
		if err = connect.GetMQConn().Publish("job-result", resultByte); err != nil {
			j.updateStatus("something wrong at MQ server, " + err.Error())
		}
	}

	j.updateStatus("done")
	return nil
}

func (j *Job) RunSync(info *oj.JobRequest) (err error) {

	if j.GetAvailable() != true {
		return errors.New("already running Job")
	}

	j.setAvailable(false)
	j.updateStatus("init")
	j.info = info
	return j.run()
}

func (j *Job) RunAsync(info *oj.JobRequest) (err error) {

	if j.GetAvailable() != true {
		return errors.New("already running Job")
	}

	j.setAvailable(false)
	j.updateStatus("init")
	j.info = info

	go j.run()
	return nil
}
